#ifndef UNIQUE_PTR_H
#define UNIQUE_PTR_H

template<class T>
class smartPtr {
private:
    T* ptr;

public:
    smartPtr(T* _ptr = nullptr) : ptr(_ptr) { };

    ~smartPtr() {
        delete ptr;
    }

    smartPtr(const smartPtr& x) = delete; // конструктор копирования

    smartPtr(smartPtr&& x) : ptr(x.ptr) { x.ptr = nullptr; };

    smartPtr& operator=(const smartPtr& x) = delete;

    smartPtr& operator=(smartPtr&& x) {

        if (&x == this)
            return *this;

        delete ptr;

        ptr = x.ptr;
        x.ptr = nullptr;

        return *this;
    }

    T& operator*() const { return *ptr; };

    T* operator->() const { return ptr; };

    bool isNull() const { return ptr == nullptr; };

    T* getObject() { return ptr; };

};

#endif // UNIQUE_PTR_H
