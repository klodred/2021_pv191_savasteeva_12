#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <iostream>
#include <QPushButton>
#include <QTextStream>
#include <QStringListModel>
#include "SmartPtr.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    void loadLinkInFile();
    ~MainWindow();

private slots:
    void SearchImageButtonPressed();

    void ShowCurrentImageButtonPressed(const QModelIndex &i);

    void SetFileNameButtonPressed();

    void ClearButtonPressed();

private:
    Ui::MainWindow *ui;

    QStringList currentFiles;

    QString fileName = "resource.txt";

    smartPtr<QStringListModel> modelCurrentFiles;
};
#endif // MAINWINDOW_H
