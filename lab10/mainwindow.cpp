#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow) {
    ui->setupUi(this);
    this->setWindowTitle(QString("Photo redactor"));
    connect(ui->ButtonSearch, SIGNAL(clicked()), this, SLOT (SearchImageButtonPressed()));
    connect(ui->ButtonSet, SIGNAL(clicked()), this, SLOT (SetFileNameButtonPressed()));
    connect(ui->ButtonClear, SIGNAL(clicked()), this, SLOT (ClearButtonPressed()));
    modelCurrentFiles = new QStringListModel(this);

    modelCurrentFiles->setStringList(currentFiles);

    ui->listView->setModel(modelCurrentFiles.getObject());
    connect(ui->listView, SIGNAL(clicked(QModelIndex)),this, SLOT(ShowCurrentImageButtonPressed(QModelIndex)));
}

MainWindow::~MainWindow() {
    loadLinkInFile();
    delete ui;
}

void MainWindow::ShowCurrentImageButtonPressed(const QModelIndex &mIndex) {
     if (mIndex.isValid()) {

         QString dataStr = mIndex.data().toString();
         QPixmap mypix (dataStr);
         mypix = mypix.scaled(ui->label_2->size(),Qt::KeepAspectRatio);
         ui->label_2->setPixmap(mypix);
     }
}

void MainWindow::ClearButtonPressed() {
    this->currentFiles.clear();
    this->modelCurrentFiles->setStringList(currentFiles);
}

void MainWindow::SearchImageButtonPressed() {
    QFileDialog dialog(this);
    dialog.setNameFilter(tr("Images (*.png *.bmp *.jpg)"));
    dialog.setFileMode(QFileDialog::ExistingFiles);
    QStringList fileNames;

    if (dialog.exec())
       fileNames = dialog.selectedFiles();

    for (int i = 0; i < fileNames.size(); ++i) {

        QString fileName = fileNames[i];
        bool find = false;

        for (int i = 0; (i < currentFiles.size()) && !find; ++i) {

            find = currentFiles[i] == fileName ? true : find;
        }

        if (!find) {

            currentFiles << fileName;
        }
    }

    modelCurrentFiles->setStringList(currentFiles);
}

void MainWindow::SetFileNameButtonPressed() {
    QString name = ui->textEdit->toPlainText().trimmed();

    if (!name.isEmpty())
        this->fileName = name;
}

void MainWindow::loadLinkInFile() {
    QFile file(this->fileName);
    file.open(QIODevice::WriteOnly);
    QTextStream out(&file);

    for (int i = 0; i < this->currentFiles.size(); ++i) {

        out << this->currentFiles[i] << "\n";
    }

    file.close();
}


