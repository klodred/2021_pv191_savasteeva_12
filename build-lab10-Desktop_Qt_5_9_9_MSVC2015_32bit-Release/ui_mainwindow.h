/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.9.9
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QPushButton *ButtonSearch;
    QListView *listView;
    QTextEdit *textEdit;
    QLabel *label;
    QPushButton *ButtonSet;
    QLabel *label_pp;
    QPushButton *ButtonClear;
    QLabel *label_2;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(964, 691);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QStringLiteral("centralwidget"));
        ButtonSearch = new QPushButton(centralwidget);
        ButtonSearch->setObjectName(QStringLiteral("ButtonSearch"));
        ButtonSearch->setGeometry(QRect(30, 20, 131, 51));
        listView = new QListView(centralwidget);
        listView->setObjectName(QStringLiteral("listView"));
        listView->setGeometry(QRect(10, 180, 256, 381));
        textEdit = new QTextEdit(centralwidget);
        textEdit->setObjectName(QStringLiteral("textEdit"));
        textEdit->setGeometry(QRect(90, 80, 131, 31));
        label = new QLabel(centralwidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 90, 55, 16));
        ButtonSet = new QPushButton(centralwidget);
        ButtonSet->setObjectName(QStringLiteral("ButtonSet"));
        ButtonSet->setGeometry(QRect(250, 80, 81, 31));
        label_pp = new QLabel(centralwidget);
        label_pp->setObjectName(QStringLiteral("label_pp"));
        label_pp->setGeometry(QRect(70, 150, 151, 21));
        QFont font;
        font.setFamily(QStringLiteral("Nirmala UI Semilight"));
        label_pp->setFont(font);
        ButtonClear = new QPushButton(centralwidget);
        ButtonClear->setObjectName(QStringLiteral("ButtonClear"));
        ButtonClear->setGeometry(QRect(20, 570, 93, 28));
        label_2 = new QLabel(centralwidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(344, 10, 611, 621));
        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QStringLiteral("menubar"));
        menubar->setGeometry(QRect(0, 0, 964, 26));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QStringLiteral("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
        ButtonSearch->setText(QApplication::translate("MainWindow", "load image", Q_NULLPTR));
        label->setText(QApplication::translate("MainWindow", "File name", Q_NULLPTR));
        ButtonSet->setText(QApplication::translate("MainWindow", "set", Q_NULLPTR));
        label_pp->setText(QApplication::translate("MainWindow", "Base image", Q_NULLPTR));
        ButtonClear->setText(QApplication::translate("MainWindow", "clear", Q_NULLPTR));
        label_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
